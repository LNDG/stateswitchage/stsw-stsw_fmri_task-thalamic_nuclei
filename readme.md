[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## PLS loadings within thalamus

Thalamic loci of behavioral PLS. To assess the thalamic loci of most reliable behavioral relations (Figure 7d), we assessed bootstrap ratios within two thalamic masks. First, for nucleic subdivisions, we used the Morel parcellation scheme as consolidated and kindly provided by Hwang et al. 41 for 3 mm data at 3T field strength. The abbreviations are as follows: AN: anterior nucleus; VM: ventromedial; VL: ventrolateral; MGN: medial geniculate nucleus; LGN: lateral geniculate nucleus; MD: mediodorsal; PuA: anterior pulvinar; LP: lateral-posterior; IL: intra-laminar; VA: ventral-anterior; PuM: medial pulvinar; Pul: pulvinar proper; PuL: lateral pulvinar. Second, to assess cortical white-matter projections we considered the overlap with seven structurally-derived cortical projection zones suggested by Horn & Blankenburg 115, which were derived from a large adult sample (N = 169). We binarized continuous probability maps at a relative 75% threshold of the respective maximum probability, and re-sliced masks to 3mm (ICBM 2009c MNI152). 

---

TO DO: add function descriptions