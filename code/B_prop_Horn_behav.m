% plot histogram of BSR voxels in Horn subregions

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.dataBSR = fullfile(rootpath, '..', 'pls_mean', 'data'); 
pn.tools = fullfile(rootpath, '..', 'pls_mean', 'tools'); 
    addpath(genpath(fullfile(pn.tools, 'NIFTI_toolbox')));
    addpath(genpath(fullfile(pn.tools, 'preprocessing_tools')));
pn.figures = fullfile(rootpath, 'figures');

% % get GM mask coordinates used in PLS
% maskpath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii';
% [mask] = double(S_load_nii_2d(maskpath));

% load BSR mask
BSR_img_unthresh = S_load_nii_2d(fullfile(pn.dataBSR, ...
    'pls','behavPLS_STSWD_SPM_YA_OA_red_1group_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded.img'))';
BSR_img_unthresh(BSR_img_unthresh==0) = NaN;

region={'occipital' 'postparietal' 'prefrontal' 'premotor' 'primarymotor' 'sensory' 'temporal'};
regionNames={'Occipital' 'Parietal' 'Prefrontal' 'Premotor' 'Motor' 'Sensory' 'Temporal'};

%%  get overlap with individual thalamic projection zones

% get  BSR voxels for each part and compute percetange voxels
BSR_Hornthal_parts=[]; prop_Hornthal_parts = [];
for p2 = 1:length(region)
    p2_part=fullfile(pn.data, 'A_standards','Horn_2016_Thalamic_Connectivity_Atlas','mixed', [region{p2} '_thr_MNI_3mm.nii']);
    HornMask=S_load_nii_2d(p2_part)';
    BSR_Hornthal_parts(p2,:)=nanmean(double(BSR_img_unthresh(logical(HornMask))));
    prop_Hornthal_parts(p2,:)=numel(find(BSR_img_unthresh(logical(HornMask))>3))/numel(find(HornMask~=0));
end


% plot
h = figure('units','centimeters','position',[.1 .1 8 6]);
set(gcf,'renderer','Painters')
cla; hold on;
[~, sortInd] = sort(BSR_Hornthal_parts, 'descend');
scatter([1:numel(region)], prop_Hornthal_parts(sortInd)*100,100, 'k', 'filled')
ylabel({'% Voxels';'BSR >= 3'})
yyaxis right
scatter([1:numel(region)], BSR_Hornthal_parts(sortInd),100, 'r', 'filled') % invert for positive loading
xlabel('Projection zone (Horn)')
ylabel('mean BSR')
xlim([.5,length(region)+.5])
xticks(1:numel(regionNames))
xticklabels(regionNames(sortInd))
xtickangle(90)
set(findall(gcf,'-property','FontSize'),'FontSize',13)

figureName = 'BehavBSR_Horn_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% SourceData

SourceData_prop = prop_Hornthal_parts(sortInd)*100;
SourceData_BSR = BSR_Hornthal_parts(sortInd);
SourceData_Names = regionNames(sortInd)';