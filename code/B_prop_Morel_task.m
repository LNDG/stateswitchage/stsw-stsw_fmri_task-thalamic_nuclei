% plot histogram of BSR voxels in Horn subregions

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.dataBSR = fullfile(rootpath, '..', 'pls_mean', 'data'); 
pn.tools = fullfile(rootpath, '..', 'pls_mean', 'tools'); 
    addpath(genpath(fullfile(pn.tools, 'NIFTI_toolbox')));
    addpath(genpath(fullfile(pn.tools, 'preprocessing_tools')));
pn.figures = fullfile(rootpath, 'figures');

% load BSR mask
BSR_img_unthresh = S_load_nii_2d(fullfile(pn.dataBSR, ...
    'pls','taskPLS_STSWD_SPM_YA_OA_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded.img'))';
BSR_img_unthresh(BSR_img_unthresh==0) = NaN;

region={'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'};
regionNames={'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'};

%%  get overlap with individual thalamic projection zones

% get  BSR voxels for each part and compute percetange voxels
BSR_regions=[]; prop_regions = [];
for ind_region = 1:length(region)
    maskpath=fullfile(pn.data, 'A_standards','Morel',[region{ind_region} '_thr_MNI_3mm.nii']);
    current_mask=S_load_nii_2d(maskpath)';
    BSR_regions(ind_region,:)=nanmean(double(BSR_img_unthresh(logical(current_mask))));
    prop_regions(ind_region,:)=numel(find(BSR_img_unthresh(logical(current_mask))>3))/numel(find(current_mask~=0));
end

% plot
h = figure('units','centimeters','position',[.1 .1 13 6]);
set(gcf,'renderer','Painters')
cla; hold on;
[~, sortInd] = sort(BSR_regions, 'descend');
scatter([1:numel(region)], prop_regions(sortInd)*100,100, 'k', 'filled')
ylabel({'% Voxels';'BSR >= 3'})
yyaxis right
scatter([1:numel(region)], BSR_regions(sortInd),100, 'r', 'filled') % invert for positive loading
xlabel('Nucleus (Morel)')
ylabel('mean BSR')
xlim([.5,length(region)+.5])
xticks(1:13)
xticklabels(regionNames(sortInd))
xtickangle(90)
set(findall(gcf,'-property','FontSize'),'FontSize',15)

figureName = 'TaskBSR_Morel_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% SourceData

SourceData_prop = prop_regions(sortInd)*100;
SourceData_BSR = BSR_regions(sortInd);
SourceData_Names = regionNames(sortInd)';