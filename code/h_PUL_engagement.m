% visualize thalamic activity

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.tools	= fullfile(rootpath, '..','pls_eventrelated', 'tools'); 
    addpath(fullfile(pn.tools, 'nifti_toolbox'));
    addpath(fullfile(pn.tools, 'preprocessing_tools'));
pn.data_BOLD = fullfile(rootpath, '..', 'pls_eventrelated', 'data', 'MeanBOLD_ER_PLS_v4');
pn.data = fullfile(rootpath, 'data');

% 2132 excluded
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

region={'PuA'; 'PuM'; 'Pul'; 'PuLL'};

BOLD_thalamus = [];
for indID = 1:numel(IDs)
    disp(num2str(indID))
    subjData = load(fullfile(pn.data_BOLD,...
        ['meanBOLD_ER_',IDs{indID},'_BfMRIsessiondata.mat']));
    % reshape
    curData = reshape(subjData.st_datamat, 9, 17,numel(subjData.st_coords));
    pulmask = [];
    for indRegion = 1:numel(region)
        mask = fullfile(rootpath, '..', 'thalamic_nuclei', 'data', 'A_standards', 'Morel', [region{indRegion}, '_thr_MNI_3mm.nii']);
        [img] = double(S_load_nii_2d(mask));
        pulmask = cat(2, plumask, img);
    end
    pulmask = sum(pulmask,2);
    BOLD_thalamus(indID,indRegion,:,:) = squeeze(nanmean(curData(:,:,logical(pulmask(subjData.st_coords))),3));
end

save(fullfile(pn.data,'BOLD_PUL.mat'), 'BOLD_thalamus', 'IDs')
