% visualize thalamic activity

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.tools	= fullfile(rootpath, '..','pls_eventrelated', 'tools'); 
    addpath(fullfile(pn.tools, 'nifti_toolbox'));
    addpath(fullfile(pn.tools, 'preprocessing_tools'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
pn.data = fullfile(rootpath, 'data');
pn.figures = fullfile(rootpath, 'figures');
pn.summary = fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

region={'AllNuclei'; 'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'};

load(fullfile(pn.data,'BOLD_thalamus.mat'), 'BOLD_thalamus', 'IDs')

ageIdx{1} = find(cellfun(@str2num, IDs, 'un', 1)<2000);
ageIdx{2} = find(cellfun(@str2num, IDs, 'un', 1)>2000);

%% baseline with prestim period

TRtime = .645.*([1:size(BOLD_thalamus,4)]-1); % minus 1 here, as the first TR captures the cue onset
idxTR = find(TRtime > 3 & TRtime < 5); % use TRs between 3 and 5 seconds as baseline
baseline = repmat(nanmean(BOLD_thalamus(:,:,:,idxTR),4),1,1,1,size(BOLD_thalamus,4));
BOLD_thalamus = (BOLD_thalamus-baseline);

%% median split thalamic response according to drift

indNucleus = 6;

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'));

idx_AttFactor = ismember(STSWD_summary.IDs,IDs);
[sortVal, sortIdx] = sort(STSWD_summary.HDDM_vat.driftEEGMRI_pc_linear_win(idx_AttFactor), 'descend'); % here it is a negative slope

lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

time = [0:16].*.645;

h = figure('units','normalized','position',[.1 .1 .6 .25]);
    subplot(1,3,1); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(highChIdx,indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '--','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', '--', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    title({'Thalamic modulation by load';'split by drift modulation'}); xlim([0 10])
    leg = legend([l1.mainLine, l3.mainLine],{'High drift mod.'; 'Low drift mod.'}, 'location', 'NorthWest'); legend('boxoff');
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,2); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(lowChIdx,indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,indNucleus,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,indNucleus,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'Low modulators'}); xlim([0 10]);ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,3); cla; hold on;
     % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(highChIdx,indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,indNucleus,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,indNucleus,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'High modulators'}); xlim([0 10]); ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',16)

    figureName = 'F_thalamicBOLD_ER';

%     saveas(h, fullfile(pn.plotFolder, figureName), 'png');
%     saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
%     close(h);

%% plot split by YA, OA

h = figure('units','normalized','position',[.1 .1 .6 .25]);
    subplot(1,3,1); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5.5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(:,indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(:,indNucleus,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(:,indNucleus,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(:,indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    xlim([0 10]);ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,2); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5.5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(ageIdx{1},indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(ageIdx{1},indNucleus,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(ageIdx{1},indNucleus,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(ageIdx{1},indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'YA'}); xlim([0 10]);ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,3); cla; hold on;
     % indicate assumed stimulus period in background
    patches.timeVec = [5.5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(ageIdx{2},indNucleus,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(ageIdx{2},indNucleus,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(ageIdx{2},indNucleus,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(ageIdx{2},indNucleus,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'OA'}); xlim([0 10]); ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',16)

    figureName = 'f_MD_yaoa_matched';
    saveas(h, fullfile(pn.figures, figureName), 'png');
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    
%     % LMEM
%     BOLD_thalamus_avg = squeeze(nanmean(BOLD_thalamus(:,indNucleus,:,time>=patches.timeVec(1) & time<=patches.timeVec(2)),4));
%     
%     tblprep = [];
%     tblprep.data = BOLD_thalamus_avg(:);
%     tblprep.sub = repmat([1:size(BOLD_thalamus_avg,1)]',1,4); tblprep.sub = tblprep.sub(:);
%     tblprep.age(ismember(tblprep.sub, ageIdx{1})) = 1;
%     tblprep.age(ismember(tblprep.sub, ageIdx{2})) = 2;
%     tblprep.age = tblprep.age';
%     tblprep.cond = repmat([1:4],size(BOLD_thalamus_avg,1),1); tblprep.cond = tblprep.cond(:);
%     tbl = table(tblprep.data,tblprep.sub,tblprep.cond, tblprep.age,'VariableNames',{'bold','sub','cond','age'});
%     tbl.age = categorical(tbl.age);
% 
%     lme = fitlme(tbl,'bold~cond*age+(cond|sub)+(age|sub)+(age*cond|sub)')

    %% plot YAOA L1, L4
    
    h = figure('units','normalized','position',[.1 .1 .6 .6]);
    for indNucleus = 1:numel(region)
        subplot(4,4,indNucleus); cla; hold on;
        % indicate assumed stimulus period in background
        patches.timeVec = [5 8];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-4 10];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        curData = squeeze(BOLD_thalamus(ageIdx{1},indNucleus,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', ':','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(BOLD_thalamus(ageIdx{1},indNucleus,2:4,:),3));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_thalamus(ageIdx{2},indNucleus,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', ':', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(BOLD_thalamus(ageIdx{2},indNucleus,2:4,:),3));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        title(region{indNucleus}); xlim([2 10])
        %leg = legend([l1.mainLine, l3.mainLine],{'High drift mod.'; 'Low drift mod.'}, 'location', 'NorthWest'); legend('boxoff');
        xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
        %set(findall(gcf,'-property','FontSize'),'FontSize',18)
    end
    
    %% plot YA, OA for MD and LGN
    
    nuclei2Plot = [6, 11];
    h = figure('units','normalized','position',[.1 .1 .2 .15]);
    for nucleus = 1:numel(nuclei2Plot)    
        subplot(1,2,nucleus); cla; hold on;
        indNucleus = nuclei2Plot(nucleus);
        % indicate assumed stimulus period in background
        patches.timeVec = [5 8];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-4 10];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        curData = squeeze(BOLD_thalamus(ageIdx{1},indNucleus,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', ':','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(BOLD_thalamus(ageIdx{1},indNucleus,4,:),3));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(BOLD_thalamus(ageIdx{2},indNucleus,1,:));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', ':', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(BOLD_thalamus(ageIdx{2},indNucleus,4,:),3));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        title(region{indNucleus}); xlim([2 10]); 
        if nucleus == 1
            ylim([-2 8]);
        elseif nucleus == 2
            ylim([-4 5]);
            leg = legend([l2.mainLine, l4.mainLine],{'YA'; 'OA'}, 'location', 'SouthEast'); legend('boxoff');
        end
        xlabel('Time (s from stim onset)'); 
        if nucleus == 1
            ylabel('BOLD magnitude (normalized)');
        end
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',15)
    
%     figureName = 'f_MD_LGN_yaoa';
%     saveas(h, fullfile(pn.figures, figureName), 'png');
%     saveas(h, fullfile(pn.figures, figureName), 'epsc');
%     close(h);

%% plot PUL for YA, OA

    nuclei2Plot = {7; 7:14};
    h = figure('units','normalized','position',[.1 .1 .2 .15]);
    for nucleus = 1:numel(nuclei2Plot)
        subplot(1,2,nucleus); cla; hold on;
        indNucleus = nuclei2Plot{nucleus};
        % indicate assumed stimulus period in background
        patches.timeVec = [5 8];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-4 10];
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        curData = squeeze(nanmean(BOLD_thalamus(ageIdx{1},indNucleus,1,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', ':','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(nanmean(BOLD_thalamus(ageIdx{1},indNucleus,4,:),2),3));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(BOLD_thalamus(ageIdx{2},indNucleus,1,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', ':', 'linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(nanmean(BOLD_thalamus(ageIdx{2},indNucleus,4,:),2),3));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
        %title(region{indNucleus}); 
        xlim([2 10]); 
        if nucleus == 1
            ylim([-2 8]);
        elseif nucleus == 2
            ylim([-2 8]);
            leg = legend([l2.mainLine, l4.mainLine],{'YA'; 'OA'}, 'location', 'SouthEast'); legend('boxoff');
        end
        xlabel('Time (s from stim onset)'); 
        if nucleus == 1
            ylabel('BOLD magnitude (normalized)');
        end
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',15)
    
    figureName = 'f_PUL_yaoa';
    saveas(h, fullfile(pn.figures, figureName), 'png');
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    close(h);