% visualize thalamic activity

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.tools	= fullfile(rootpath, '..','pls_eventrelated', 'tools'); 
    addpath(fullfile(pn.tools, 'nifti_toolbox'));
    addpath(fullfile(pn.tools, 'preprocessing_tools'));
pn.data_BOLD = fullfile(rootpath, '..', 'pls_eventrelated', 'data', 'MeanBOLD_ER_PLS_v4');
pn.data = fullfile(rootpath, 'data');
pn.summary = fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

region={'AllNuclei'; 'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'};

load(fullfile(pn.data,'BOLD_thalamus.mat'), 'BOLD_thalamus', 'IDs')

%% median split thalamic response according to drift

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'));

idx_AttFactor = ismember(STSWD_summary.IDs,IDs);
[sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftMRI_linear(idx_AttFactor), 'descend'); % here it is a negative slope

lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

% add within-subject error bars
pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);

time = [0:16].*.645;

h = figure('units','normalized','position',[.1 .1 .6 .25]);
set(gcf,'renderer','Painters')
    subplot(1,3,1); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(highChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '--','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k', 'linestyle', '--', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    title({'Thalamic modulation by load';'split by drift modulation'}); xlim([0 10])
    leg = legend([l1.mainLine, l3.mainLine],{'High drift mod.'; 'Low drift mod.'}, 'location', 'NorthWest'); legend('boxoff');
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,2); cla; hold on;
    % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(lowChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.8 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.6 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.4 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(lowChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [.2 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'Low modulators'}); xlim([0 10]);ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,3,3); cla; hold on;
     % indicate assumed stimulus period in background
    patches.timeVec = [5 8];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-2 10];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    curData = squeeze(BOLD_thalamus(highChIdx,1,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .8 .8],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,2,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .6 .6],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,3,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .4 .4], 'linestyle', '-', 'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BOLD_thalamus(highChIdx,4,:));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', [1 .2 .2],'linestyle', '-','linewidth', 2}, 'patchSaturation', .1);
    legend([l1.mainLine,l2.mainLine, l3.mainLine, l4.mainLine],{'1 Target'; '2 Targets'; '3 Targets'; '4 Targets'}, 'location', 'NorthWest'); legend('boxoff');
    title({'High modulators'}); xlim([0 10]); ylim([-1 8]);
    xlabel('Time (s from stim onset)'); ylabel('BOLD magnitude (normalized)');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',16)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/C_figures/';
    figureName = 'F_thalamicBOLD_ER_OA';
